
Analysis Workflow
=================

ebtrack's main goal is to help you scan through your recordings quickly. Your recordings
have to be tiff stacks or a sequence of tiff files (not tested yet, any feedback appreciated).

In general, all command line programs provide information on available parameters and options
with ``<name-of-command> --help``. E.g., for the command ``ebtrack-extract-trajectories``
type

.. code:: bash

   $ ebtrack-extract-trajectories --help

Extract Trajectories
--------------------

Let's say you have a recording of 600 frames of the egyptian blue channel in file
``egyptian_blue.tif``. Before ebtrack can perform any other analysis, you need to
extract the trajectories of egyptian blue particles. Use the command

.. code:: bash

   $ ebtrack-extract-trajectories egyptian_blue.tif

This will start an interactive plot, where you can adjust the tracking parameters. It should
look something like this:

.. image:: _static/images/param_selector.png
   :width: 90%

Adjust diameter and mass (press enter or leave text box for new values to take effect),
and check in a few frames (click on frame slider) that all particles are found correctly.

Before you close the window, check the values for memory, max. dist. (maximum distance
a particle can move between frames) and the minimum number of frames a particle has to
be found in.

When you close the window, you need to confirm that you want to extract trajectories with
the current parameters. Extracted trajectories will be written into a folder
``egyptian_blue_ebtrack_results/analysis_000``. The first folder name is derived
from the file name without file extension. The second folder is always named ``analysis``
with an index suffix. If you execute the extract trajectories command again, it will be
written to ``egyptian_blue_ebtrack_results/analysis_001``.

For the following programs for further analysis, you usually
**have to provide the path to an analysis folder** as first argument. The trajectories in
that analysis folder will then be used.

Compute MSD
-----------

To compute the MSD and fit the slope, use the program ``ebtrack-compute-msd``:

.. code:: bash

   $ ebtrack-compute-msd egyptian_blue_ebrack_results/analysis_000 100 -mpp 0.123 -fps 10.0

The second argument is the maximum lag time (in frames) used for the fit of the slope.
The program produces imsd plots (individual MSDs), imsd plots with fits,
and an ensemble MSD plot. All plots are saved to the analysis folder.

Additionaly, the results of the fit are saved to CSV files to the analysis folder.
Notation for the results is taken from trackpy. Thus, :math:`n` is the exponent of
the power law fit, i.e. the slope of the MSD in the log-log plot. The second
parameter :math:`A` is 4 times the diffusivity, :math:`A = 4D`.

The third and fourth arguments, ``mpp`` (microns per pixel)
and ``fps`` (frames per second) only need to be specified once.
For following analyses that require these physical conversion parameters, they will
be read from the ``ebtrack_results`` folder.
**If you specified the wrong parameters here, make sure to specify the correct ones on the next program execution!** Otherwise you might do all other analyses with wrong conversions.

Find Nuclei
-----------

It's assumed that nuclei positions don't change over one recording. So you should have one
static background file for the nucleus channel like this one:

.. figure:: _static/images/background.png

   Let's assume the background is saved in ``background.tif``.

Execute the command

.. code:: bash

   $ ebtrack-find-nuclei egyptian_blue_ebtrack_results/analysis_000 background.tif

This will first open an interactive plot like the one for trajectory extraction. But that is only
the first step to try to find as many nuclei as possible automatically, but it's usually not
working that well.
After you close it, plots with an excerpt for each trajectory will pop up. You can click
on the image to mark/unmark nucleus positions.

Nuclei are hard to see. You can specify parameters ``-minb`` and ``-maxb``, minimum and
maximum brightness. You can use ImageJ to find suitable values for those. Specify these arguments
to make spotting nuclei a bit better.

**This will probably be reworked to rather rely on ImageJ for marking the positions, otherwise we might have to implement all their nice features for contrast and brightness settings**.

Positions of nuclei are saved as CSV into the specified analysis folder.

Proximity Analysis
------------------

Now that we have nucleus positions and trajectories of egyptian blue, we can combine
both to see if dynamics are affected by proximity to an nucleus.

**So far this program only masks part of trajectories as being close to an nucleus!** No
further analysis happening so far.

Run the program with

.. code:: bash

   $ ebtrack-trajectory-nucleus-analysis egyptian_blue_ebtrack_results/analysis_000 1.0 -bg background.tif

Plots are saved to ``egyptian_blue_ebtrack_results/analysis_000/traj_plus_nuclei``.
   
Velocity Plots
--------------

This program plots the trajectories colored for velocity.
E.g., run the command like this:

.. code:: bash

   $ ebtrack-velocity-plots egyptian_blue_ebtrack_results/analysis_000 background.tif

Plots are saved to ``egyptian_blue_ebtrack_results/analysis_000/velocity_plots``.


Vanhove Plot
------------

Create Vanhove plots with

.. code:: bash

   $ ebtrack-vanhove-plot egyptian_blue_ebtrack_results/analysis_000

**Work in Progress** ... if you need Vanhove plots, ask Ilyas for help!
