"""
USAGE: 
   o install in develop mode: navigate to the folder containing this file,
                              and type 'pip install -e . --user'.
                              (omit '--user' if you want to install for
                               all users)                           
"""
from setuptools import setup

setup(name='ebtrack',
      version='0.2.1',
      description='Collection of programs for tracking and analysing trajectories of sheets of egyptian blue.',
      url='',
      author=['Ilyas Kuhlemann', 'Gabriele Selvaggio', 'Niklas Herrmann'],
      author_email='ikuhlem@gwdg.de',
      license='GPLv3',
      packages=["ebtrack"],
      entry_points={
          "console_scripts": ['ebtrack-extract-trajectories=ebtrack.extract_trajectories:main',
                              'ebtrack-compute-msd=ebtrack.compute_msd:main',
                              'ebtrack-vanhove-plot=ebtrack.vanhove_plot:main',
                              'ebtrack-find-nuclei=ebtrack.find_nuclei:main',
                              'ebtrack-trajectory-nucleus-analysis=ebtrack.trajectory_nucleus_analysis:main',
                              'ebtrack-velocity-plots=ebtrack.velocity_plots:main'], 
          "gui_scripts": [
          ]
      },
      install_requires=['matplotlib', 'trackpy', 'pims', 'pillow', 'click', 'numba', 'pandas'],
      zip_safe=False)
