.. ebtrack documentation master file, created by
   sphinx-quickstart on Tue Oct  1 09:39:32 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ebtrack's documentation!
===================================

Some tools for tracking of egyptian blue in a layer of drosophila embryos.
Specifically designed to match experiments in `this preprint <https://doi.org/10.1101/710384>`_,
but can probably be useful for other tracking as well.

All of it relies on great libraries like numpy, pandas, trackpy, matplotlib, click etc.
We are very greatful for all developers of python and these great projects! Keep up the good work!

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   workflow
	     
License
=======

Copyright (c) 2019 Ilyas Kuhlemann, Gabriele Selvaggio, Niklas Herrmann

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see `<https://www.gnu.org/licenses/>`_.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
