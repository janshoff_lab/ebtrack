from typing import Iterable, Tuple
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def get_bounding_box(x, y, d, h, w):
    xmin = max(np.nanmin(x)-d, 0)
    xmax = min(np.nanmax(x)+d, w)
    ymin = max(np.nanmin(y)-d, 0)
    ymax = min(np.nanmax(y)+d, h)
    return [(xmin, xmax), (ymin, ymax)]

def invert_yaxis(ax: plt.Axes):
    bottom, top = ax.get_ylim()
    if top > bottom:
        ax.set_ylim(top, bottom, auto=None)
    return ax

def convert_traj_dataframe_to_array(traj: pd.DataFrame) -> np.ndarray:
    posx = traj.set_index(['frame', 'particle'])['x'].unstack()
    posy = traj.set_index(['frame', 'particle'])['y'].unstack()
    xy = np.full((posx.index.max()+1, posx.shape[1], 2), np.nan)
    for i, pid in enumerate(posx):
        xy[posx.index, i, 0] = posx[pid]
        xy[posx.index, i, 1] = posy[pid]
    return xy

def get_displacements(files_traj: Iterable[str],
                      microns_per_pixel: float,
                      lag: int) -> Tuple[np.ndarray, np.ndarray]:
    """
    Get displacements from a list of trajectory files.
    """
    displacements_x = []
    displacements_y = []    
    for f in files_traj:
        traj = pd.read_csv(f, index_col=None)
        xy = convert_traj_dataframe_to_array(traj)
        xy = xy * microns_per_pixel
        displacements_f = xy[lag:] - xy[:-lag]
        dx = displacements_f[:, :, 0]
        dy = displacements_f[:, :, 1]
        displacements_x.append(dx[np.isfinite(dx)])
        displacements_y.append(dy[np.isfinite(dy)])
    displacements_x = np.concatenate(displacements_x)
    displacements_y = np.concatenate(displacements_y)
    return displacements_x, displacements_y

