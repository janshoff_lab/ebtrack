import os
import click
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import pandas as pd
import trackpy as tp
from trackpy.utils import fit_powerlaw
from . import results_folder as rf

mpl.rc('lines', linewidth=2.5, markersize=2.2)


def compute_msd(analysis_folder, max_lag_fit: int, min_lag_fit: int,
                microns_per_pixel, frames_per_second):
    
    print("{:=^80}".format(' ebtrack-compute-msd '))
    fname_traj = os.path.join(analysis_folder, 'trajectories.csv')
    trajectories = pd.read_csv(fname_traj)

    results_folder = os.path.abspath(
        os.path.join(analysis_folder, '..'))

    if microns_per_pixel is None or frames_per_second is None:
        try:
            mpp, fps = rf.load_conversion_parameters(results_folder)
        except FileNotFoundError:
            msg = ''.join(["Tried to load conversion parameters (microns per ",
                           "pixel and frames per second) from results folder, ",
                           "but they were not found. \n",
                           "Looks like you did not specify them before. ",
                           "You need to specify them at least once. ",
                           "From then on, ebtrack will read them from that file.",
                           "If you provide them again, the old ones will be ",
                           "replaced."])
            print(msg)
            return
        if microns_per_pixel is None:
            microns_per_pixel = mpp
        if frames_per_second is None:
            frames_per_second = fps
    rf.save_conversion_parameters(results_folder, microns_per_pixel, frames_per_second)
    
    imsd = tp.imsd(trajectories, microns_per_pixel,
                   frames_per_second, max_lagtime=len(trajectories))
    
    fname_imsd = os.path.join(analysis_folder,
                              'imsd.csv')
    fname_imsd_fit = os.path.join(analysis_folder,
                                  'imsd_fit_min_lag_{}_max_lag_{}.csv'.format(min_lag_fit, max_lag_fit))
    imsd.to_csv(fname_imsd)
    print("wrote imsd results to '{}'".format(fname_imsd))

    # ================== plot without fit ======================================
    fig, ax = plt.subplots(1, 1)
    plot_msd_and_fit(ax, imsd, min_lag_fit, max_lag_fit, plot_fit=False)
    fname_fig = os.path.splitext(fname_imsd)[0] + '.pdf'
    fig.savefig(fname_fig)
    print("created figure '{}'".format(fname_fig))
    
    # ================== plot with fit =========================================
    fig, ax = plt.subplots(1, 1)
    fit_results = plot_msd_and_fit(ax, imsd, min_lag_fit, max_lag_fit)
    fname_fig = os.path.splitext(fname_imsd_fit)[0] + '.pdf'
    fig.savefig(fname_fig)
    print("created figure '{}'".format(fname_fig))
    fit_results.to_csv(fname_imsd_fit)

    # ================== plot emsd =============================================
    _emsd = tp.emsd(trajectories, microns_per_pixel, frames_per_second)
    emsd = pd.DataFrame(index=_emsd.index, columns=['emsd'])
    emsd['emsd'] = _emsd
    fname_emsd = fname_imsd.replace('imsd', 'emsd')
    emsd.to_csv(fname_emsd)
    print("wrote emsd results to '{}'".format(fname_emsd))

    fig, ax = plt.subplots(1, 1)
    fit_emsd = plot_msd_and_fit(ax, emsd, min_lag_fit, max_lag_fit, plot_fit=True)
    ax.set(ylabel=r'$\langle \Delta r^2 \rangle$ [$\mu$m$^2$]',
           xlabel='lag time $t$/s',
           xscale='log',
           yscale='log')
    ax.get_legend().remove()
    ax.set_title('Ensemble MSD')

    fname_emsd_fit = fname_imsd_fit.replace('imsd', 'emsd')
    fit_emsd.to_csv(fname_emsd_fit)

    fname_fig = os.path.splitext(fname_emsd_fit)[0] + '.pdf'
    fig.savefig(fname_fig)
    print("created figure '{}'".format(fname_fig))
    plt.show()


def plot_msd_and_fit(ax, imsd: pd.DataFrame, min_fit_lagtime, max_fit_lagtime, plot_fit=True):
    results = pd.DataFrame(index=imsd.columns, columns=['n', 'A'])
    if plot_fit:
        plot_style = 'o'
    else:
        plot_style = '-'
        
    for i, particle_id in enumerate(imsd):
        color='C{}'.format(i)
        ax.plot(imsd.index, imsd[particle_id], plot_style,
                label='particle {}'.format(particle_id),
                color=color)
        y = imsd[particle_id].iloc[min_fit_lagtime: max_fit_lagtime]
        y = y[~np.isnan(y)]
        x = y.index
        # slope, intercept, r, p, stderr = linregress(np.log(x), np.log(y))
        r = fit_powerlaw(y, plot=False) # , label='particle {}'.format(particle_id))
        results.loc[particle_id] = [r['n'][particle_id], r['A'][particle_id]]
        if plot_fit:
            ax.plot(x, r['A'][particle_id]*x**r['n'][particle_id], color=color)
    ax.set(ylabel=r'$\langle \Delta r^2 \rangle$ [$\mu$m$^2$]',
           xlabel='lag time $t$/s',
           xscale='log',
           yscale='log')
    ax.legend()
    return results


@click.command()
@click.argument('analysis_folder')
@click.argument('max_lag_fit', type=int)
@click.option('--min-lag-fit', '-mf', type=int, default=1)
@click.option('--microns-per-pixel', '-mpp', type=float, default=None,
              help='If not specified, will try to load from results folder.')
@click.option('--frames-per-second', '-fps', type=float, default=None,
              help='If not specified, will try to load from results folder.')
def main(analysis_folder, max_lag_fit, min_lag_fit, microns_per_pixel, frames_per_second):
    compute_msd(analysis_folder, max_lag_fit, min_lag_fit, microns_per_pixel, frames_per_second)
    

if __name__ == "__main__":
    main()
