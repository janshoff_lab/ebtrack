
Install
=======

The repository of the project is at `<https://gitlab.gwdg.de/janshoff_lab/ebtrack>`_. Code
is **not uploaded** to `<https://pypi.org/>`_ so far. The easiest way to install is via pip.
In a command line, execute:

.. code:: bash

   $ pip install git+https://gitlab.gwdg.de/janshoff_lab/ebtrack

This should install all requirements and ebtrack itself. You should consider installing it
as a user, with:

.. code:: bash

   $ pip install --user git+https://gitlab.gwdg.de/janshoff_lab/ebtrack

On Linux, this will install everything into ``~/.local``. For the user install, you don't need admin permissions, and you don't mess with your system's python packages. The downside is, for ebtrack's command line programs to work, you might have to add ``~/.local/bin`` to your ``PATH``. If you don't know how to,
`google for it <https://www.google.com/search?q=linux+add+to+path&oq=linux+add+to+path&aqs=chrome..69i57.2800j0j7&sourceid=chrome&ie=UTF-8>`_, there are tons of tutorials available.
