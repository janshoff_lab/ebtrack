import logging
import os
import click
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
import pandas as pd

from .utils import get_bounding_box
from . import results_folder as rf

def velocity_plots(ax, trajectories: pd.DataFrame,
                   microns_per_pixel: float,
                   frames_per_second: float,
                   global_norm=None,
                   lag=1):
    
    posx = trajectories.set_index(['frame', 'particle'])['x'].unstack()[::lag]
    posy = trajectories.set_index(['frame', 'particle'])['y'].unstack()[::lag]

    xy = np.full(((posx.index.max())//lag+1, posx.shape[1], 2), np.nan)
    
    for i, pid in enumerate(posx):
        xy[posx.index//lag, i, 0] = posx[pid]
        xy[posx.index//lag, i, 1] = posy[pid]

    xy = xy * microns_per_pixel

    v = xy[1:] - xy[:-1]
    v = np.sqrt((v**2).sum(2))
    v = v * frames_per_second

    colors = np.zeros_like(v)
    colors[np.isfinite(v)] = v[np.isfinite(v)]    
    if global_norm is None:
        norm = plt.Normalize(colors.min(), colors.max())
    else:
        norm = global_norm
    line_with_max = None
    current_max = 0
        
    for i in range(xy.shape[1]):
        points = np.array([xy[:, i, 0], xy[:, i, 1]]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)

        lc = LineCollection(segments, cmap='cool', norm=norm)
        lc.set_array(colors[:, i])
        lc.set_linewidth(2)
        line = ax.add_collection(lc)
        
        if np.nanmax(v[:, i]) > current_max:
            current_max = v[:, i].max()
            line_with_max = line
    return line_with_max, norm

def save_files(outfolder, fig, ax, xy, posx, w, h, bounding_box):
    logging.info('save_files: writing ... ')
    fig.savefig(os.path.join(outfolder, 'all.pdf'))    

    for i, pid in enumerate(posx):
        bb = get_bounding_box(xy[:, i, 0], xy[:, i, 1], bounding_box, h, w)
        ax.set(xlim=bb[0],
               ylim=bb[1])
        fig.savefig(os.path.join(outfolder, 'particle_{}.pdf'.format(pid)))

    ax.set(xlim=(0, w),
           ylim=(h, 0))
    logging.info('save_files: done.')

    
@click.command()
@click.argument('analysis_folder', type=click.Path(exists=True))
@click.argument('background_file', type=click.Path(exists=True))
@click.option('--bounding-box', '-bb', type=int, default=30)
@click.option('--lag', '-l', type=int, default=1)
@click.option('--min-brightness', '-minb', type=float, default=None)
@click.option('--max-brightness', '-maxb', type=float, default=None)
@click.option('--microns-per-pixel', '-mpp', type=float, default=None,
              help='If not specified, will try to load from results folder.')
@click.option('--frames-per-second', '-fps', type=float, default=None,
              help='If not specified, will try to load from results folder.')
@click.option('-cmap', default=None,
              help='Matplotlib colormap specifier. Will plot background as green channel' +
              ' if not none is specified.')
@click.option('--global-colorbar/--no-global-colorbar', default=True)
def main(analysis_folder, background_file, bounding_box, lag, min_brightness,
         max_brightness, microns_per_pixel, frames_per_second, cmap,
         global_colorbar):
    print("{:=^80}".format(' ebtrack-velocity-plots '))
    logging.basicConfig(format="%(message)s", level=logging.INFO)
    mpl.rc('figure', figsize=(8.5, 6))

    results_folder = os.path.abspath(
        os.path.join(analysis_folder, '..'))

    if microns_per_pixel is None or frames_per_second is None:
        try:
            mpp, fps = rf.load_conversion_parameters(results_folder)
        except FileNotFoundError:
            msg = ''.join(["Tried to load conversion parameters (microns per ",
                           "pixel and frames per second) from results folder, ",
                           "but they were not found. \n",
                           "Looks like you did not specify them before. ",
                           "You need to specify them at least once. ",
                           "From then on, ebtrack will read them from that file.",
                           "If you provide them again, the old ones will be ",
                           "replaced."])
            print(msg)
            return
        if microns_per_pixel is None:
            microns_per_pixel = mpp
        if frames_per_second is None:
            frames_per_second = fps
    rf.save_conversion_parameters(results_folder, microns_per_pixel, frames_per_second)

    trajectories = pd.read_csv(os.path.join(analysis_folder, 'trajectories.csv'))
    background = plt.imread(background_file)

    pxy, pxx = background.shape
    extent = [-0.5 * microns_per_pixel, (pxx-0.5) * microns_per_pixel,
              (pxy-0.5) * microns_per_pixel, -0.5 * microns_per_pixel]
    
    background = background.copy()
    if background.ndim == 3:
        background = background[:, :, 1]
    if min_brightness is not None:
        background[ background < min_brightness ] = min_brightness
    if max_brightness is not None:
        background[ background > max_brightness ] = max_brightness

    fig, ax, line_with_max, norm = create_decorated_plot(background,
                                                         trajectories,
                                                         lag,
                                                         microns_per_pixel,
                                                         frames_per_second,
                                                         cmap)

    if global_colorbar:
        global_line_with_max = line_with_max
        global_norm = norm
    else:
        global_line_with_max = None
        global_norm = None
        
    
    posx = trajectories.set_index(['frame', 'particle'])['x'].unstack()
    posy = trajectories.set_index(['frame', 'particle'])['y'].unstack()
    for i, pid in enumerate(posx):
        fig_i, ax_i, _, __ = create_decorated_plot(background,
                                                   trajectories[trajectories['particle'] == pid],
                                                   lag,
                                                   microns_per_pixel,
                                                   frames_per_second, cmap,
                                                   global_line_with_max,
                                                   global_norm)
        x = posx[pid] * microns_per_pixel
        y = posy[pid] * microns_per_pixel
        bb = get_bounding_box(x, y, 50*microns_per_pixel, extent[2], extent[1])
        ax_i.set(xlim = bb[0],
                 ylim = [bb[1][1], bb[1][0]])                
            
    plt.show()

    print('-'*80)

def create_decorated_plot(background, trajectories, lag,
                          microns_per_pixel, frames_per_second, cmap,
                          global_line_with_max=None, global_norm=None):
    pxy, pxx = background.shape
    extent = [-0.5 * microns_per_pixel, (pxx-0.5) * microns_per_pixel,
              (pxy-0.5) * microns_per_pixel, -0.5 * microns_per_pixel]
    fig, ax = plt.subplots(1, 1)
    if cmap is None:
        bg = np.zeros((background.shape[0], background.shape[1], 3))
        bg[:, :, 1] = background/background.max()
        ax.imshow(bg, extent=extent)
    else:
        ax.imshow(background, cmap=cmap, extent=extent)    

    line_with_max, norm = velocity_plots(ax, trajectories,
                                         microns_per_pixel, frames_per_second,
                                         global_norm, lag)
    ax.set(xlabel='$x/\mu$m',
           ylabel='$y/\mu$m')
    if global_line_with_max is None:
        cb = fig.colorbar(line_with_max, ax=ax)
    else:
        cb = fig.colorbar(global_line_with_max, ax=ax)
    fig.tight_layout()
    cb.set_label('$\mu$m/s')
    return fig, ax, line_with_max, norm


if __name__ == "__main__":
    main()
