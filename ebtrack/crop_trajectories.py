import matplotlib.pyplot as plt
import pandas as pd

from .utils import get_bounding_box

def crop_trajectories(trajectories: pd.DataFrame, background_file: str, boundingbox: int):
    """
    Crop background file around trajectories.
    """
    img = plt.imread(background_file)
    
    posx = trajectories.set_index(['frame', 'particle'])['x'].unstack()
    posy = trajectories.set_index(['frame', 'particle'])['y'].unstack()    

    for pid in posx:
        x = posx[pid]
        y = posy[pid]

        bb = get_bounding_box(x, y, boundingbox, img.shape[0], img.shape[1])

        crop = img[bb[0][0]: bb[0][1], bb[1][0]: bb[1][1]]

        yield crop
