from typing import Union, Tuple
import os
import glob
import warnings
import click
import pims
import trackpy as tp
import matplotlib as mpl
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.widgets import Slider, TextBox

from .tracking_parameters import TrackingParameters
from . import results_folder as rf


Frames = Union[pims.ImageSequence, pims.TiffStack]

    
def extract_trajectories(frames, tracking_parameters) -> pd.DataFrame:
    tparams = tracking_parameters
    features = tp.batch(frames, tparams.diameter,
                        minmass=tparams.mass, invert=False,
                        engine='numba')
    traj = tp.link_df(features, tparams.max_distance,
                      memory=tparams.memory)

    trajectories = tp.filter_stubs(traj, tparams.traj_min_n_frames)
    return trajectories

def set_initial_parameters(results_folder, diameter,
                           mass, memory,
                           max_distance,
                           traj_min_n_frames) -> Tuple[pd.DataFrame,
                                                       TrackingParameters]:
    param_log = rf.load_parameter_csv(results_folder)
    last_params = rf.load_last_tracking_parameters(param_log)    

    if diameter is None:
        diameter = last_params.diameter
    if mass is None:
        mass = last_params.mass
    if memory is None:
        memory = last_params.memory
    if max_distance is None:
        max_distance = last_params.max_distance
    if traj_min_n_frames is None:
        traj_min_n_frames = last_params.traj_min_n_frames

    parameters = TrackingParameters(diameter, mass,
                                    memory, max_distance, traj_min_n_frames)
    return param_log, parameters


def load_frames(file_pattern):
    files = sorted(glob.glob(file_pattern))
    if len(files) == 0:
        msg = "No files were found with file pattern or name '{}'"
        raise FileNotFoundError(msg.format(file_pattern))
    if len(files) == 1:
        if _is_tiff(files[0]):
            frames = pims.TiffStack(files[0], as_grey=True)
        elif _is_video(files[0]):
            frames = pims.Video(files[0])
        else:
            msg = "File is not a tiff (allowed extensions = ['.tif', '.tiff']),\n"
            msg += "or a video (allowed extensions = ['avi']).\n"
            msg += "Other formats are not supported yet. Mail me to ikuhlem@gwdg.de "
            msg += "if you want your format added."
            raise RuntimeError(msg)
    else:
        frames = pims.ImageSequence(files)
    return files, frames


def _is_tiff(fname: str) -> bool:
    allowed_extensions = ['.tif', '.tiff']
    extension = os.path.splitext(fname)[1]
    if extension.lower() in allowed_extensions:
        return True
    return False


def _is_video(fname: str) -> bool:
    allowed_extensions = ['.avi']
    extension = os.path.splitext(fname)[1]
    if extension.lower() in allowed_extensions:
        return True
    return False


class InteractiveParameterFinder:

    def __init__(self, parameters):
        self.parameters = parameters
        self._current_frame = 0
    
    def start(self, frames: Frames):
        self.frames = frames
        # ipdb.set_trace()
        self.fig, (self.aximg, self.axhist) = plt.subplots(1, 2)
        # self.img = self.ax.imshow(self.frames[self._current_frame], cmap='gray')
        self.fig.tight_layout()
        self.fig.subplots_adjust(bottom=0.42)
        self.fig.canvas.mpl_connect('button_press_event', self._onclick)
        # ====================== set up frame slider ===========================
        axcolor = 'salmon'
        axframe = self.fig.add_axes([0.15, 0.35, 0.65, 0.03], facecolor=axcolor)
        sframe = Slider(axframe, 'Frame', 0, len(self.frames), valstep=1, valfmt='%d',
                        color='lightblue')
        sframe.on_changed(self._set_current_frame)
        # ====================== set up diameter text box ======================
        axbox_d = self.fig.add_axes([0.15, 0.25, 0.3, 0.075])
        self.box_diameter = TextBox(axbox_d, 'diameter',
                                    initial=str(self.parameters.diameter))
        self.box_diameter.on_submit(self._set_diameter)
        # ====================== set up mass text box ==========================
        axbox_m = self.fig.add_axes([0.6, 0.25, 0.3, 0.075])
        box_mass = TextBox(axbox_m, 'mass', initial=str(self.parameters.mass))
        box_mass.on_submit(self._set_mass)
        # ====================== set up  boxes for memory and max_distance =====
        self.fig.text(0.1, 0.21, ' '.join(['The following values won\'t change the plots,',
                                           'but will be used for tracking later:']),
                      size=12, color='darkred')
        axbox_mem = self.fig.add_axes([0.15, 0.12, 0.3, 0.075])
        box_mem = TextBox(axbox_mem, 'memory', initial=str(self.parameters.memory))
        box_mem.on_submit(self._set_memory)
        axbox_md = self.fig.add_axes([0.6, 0.12, 0.3, 0.075])
        box_md = TextBox(axbox_md, 'max. dist.', initial=str(self.parameters.max_distance))
        box_md.on_submit(self._set_max_distance)
        # ====================== set up traj-min-nframes text box ==============
        axbox_trajn = self.fig.add_axes([0.35, 0.02, 0.3, 0.075])
        box_trajn = TextBox(axbox_trajn, 'traj. min. number of frames',
                            initial=str(self.parameters.traj_min_n_frames))
        box_trajn.on_submit(self._set_trajn)
        
        self.update()
        plt.show(block=True)

    def _onclick(self, event):
        ax = event.inaxes
        if ax is None:
            return
        if ax != self.axhist:
            return
        self.parameters.mass = event.xdata
        self.update()

    def _set_trajn(self, value_str):
        self.parameters.traj_min_n_frames = int(value_str)

    def _set_memory(self, value_str):
        self.parameters.memory = int(value_str)

    def _set_max_distance(self, value_str):
        self.parameters.max_distance = float(value_str)
        
    def _set_mass(self, value_str):
        self.parameters.mass = float(value_str)
        self.update()
                
    def _set_diameter(self, value_str):
        d = int(value_str)
        if d%2 == 0:
            msg = "You set an even number for diameter, which is invalid! "
            msg += "It was now incremented by 1."
            print(msg)
            d += 1
            self.box_diameter.set_val(str(d))
        self.parameters.diameter = d
        self.update()

    def _set_current_frame(self, value):
        self._current_frame = value        
        self.update()
        
    def update(self):
        frame = self.frames[self._current_frame]
        #self.img.set_data(frame)
        self.aximg.clear()
        self.axhist.clear()
        features = tp.locate(frame, self.parameters.diameter,
                             minmass=self.parameters.mass)
        tp.annotate(features, frame, invert=False, ax=self.aximg,
                    imshow_style={'origin': 'lower'})
        self.axhist.hist(features['mass'], bins=20)
        self.fig.canvas.draw()        
        

@click.command()
@click.argument('file-pattern')
@click.option('--diameter', '-d', type=int, default=None,
              help=' '.join(['Set initial diameter in pixels used for trackpy\'s locate function.',
                             'Needs to be an odd integer!']))
@click.option('--mass', '-m', type=float, default=None,
              help="Set initial minimum mass for trackpy's locate function.")
@click.option('--memory', '-mem', type=int, default=None,
              help="Set memory (number of frames) for trackpy's link_df function.")
@click.option('--max-distance', '-md', type=float, default=None,
              help=' '.join(['Maximum distance (in pixels) particles can move between frames,',
                             'will be passed on to trackpy\'s link_df function.']))
@click.option('--traj-min-n-frames', '-t', type=int, default=None,
              help='Trajectories with less frames will be filtered out.')
def main(file_pattern, diameter, mass, memory, max_distance, traj_min_n_frames):
    print("{:=^80}".format(" ebtrack-extract-trajectories "))
    mpl.rc('figure', figsize=(10, 8))
    mpl.rc('image', cmap='gray')
    warnings.simplefilter(action='ignore', category=FutureWarning)

    files, frames = load_frames(file_pattern)
        
    results_folder = rf.get_results_folder(files[0])
    param_log, parameters = set_initial_parameters(results_folder,
                                                   diameter, mass,
                                                   memory, max_distance,
                                                   traj_min_n_frames)

    param_finder = InteractiveParameterFinder(parameters)
    param_finder.start(frames)
    parameters.save_to_default_file()

    print()
    txt = "set parameters to:\n{}"
    print(txt.format(parameters.as_string()))
    
    choice = input("track trajectories now with these parameters? [Y/n]: ")
    if not choice:
        choice = 'y'
    print()
    if choice.lower().strip() != 'y':
        return
    idx_analysis = rf.add_tracking_parameters(param_log, parameters)
    ana_folder = rf.get_analysis_folder(results_folder, idx_analysis)
    parameters.save_to_file(os.path.join(ana_folder, 'tracking_parameters.json'))
    rf.save_parameter_csv(param_log, results_folder)
    fname_traj = os.path.join(ana_folder, 'trajectories.csv')
    if os.path.exists(fname_traj):
        choice = input(' '.join(["Trajectory file for your set of parameters already exits.\n",
                                 "Continue and overwrite? [y/N]: "]))
        if choice.lower().strip() != 'y':
            return
    print("tracking particles ... ", end='')

    trajectories = extract_trajectories(frames, parameters)

    print('done')
    print("got {} trajectories".format(trajectories['particle'].nunique()))
    print("")
    print("saving to file '{}'".format(fname_traj))
    trajectories.to_csv(fname_traj)



if __name__ == "__main__":
    main()
