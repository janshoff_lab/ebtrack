import os
from .tracking_parameters import TrackingParameters


def get_trajectory_fname(input_fname: str, diameter, mass, memory,
                         max_distance, traj_min_n_frames) -> str:
    fname_wo_ext, ext = os.path.splitext(input_fname)
    if ext == '.csv':
        fname_traj = input_fname
    else:
        param_fname = TrackingParameters.get_param_filename(input_fname)
        parameters = TrackingParameters(param_fname, diameter, mass, memory,
                                        max_distance, traj_min_n_frames)
        results_folder = fname_wo_ext + "_ebtrack_results"
        fname_traj = os.path.join(results_folder,
                                  "trajectories" + parameters.get_file_suffix() + '.csv')
        if not os.path.exists(fname_traj):
            msg = "For specified file '{}' no result file exists for given parameter set.\n"
            msg += "Parameter set was:\n{}\n".format(parameters.as_string())
            msg += "With that filename '{}' was deduced, but file does not exist".format(fname_traj)
            raise FileNotFoundError(msg)
    return fname_traj
