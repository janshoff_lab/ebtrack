import os
import pandas as pd
import click
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

from .utils import get_bounding_box, invert_yaxis

def find_nuclei(analysis_folder, background_file, trajectory_bounding_box,
                min_brightness, max_brightness):
    fname_traj = os.path.join(analysis_folder, 'trajectories.csv')
    trajectories = pd.read_csv(fname_traj)

    img = plt.imread(background_file)
    if min_brightness is not None:
        img = img.copy()
        img[img < min_brightness] = min_brightness
    if max_brightness is not None:
        img = img.copy()
        img[img > max_brightness] = max_brightness
    nf = InteractiveNucleiFinder(trajectory_bounding_box, min_brightness, max_brightness)
    nf.start(trajectories, img)
    fname_pos = os.path.join(analysis_folder, 'nuclei_positions.csv')    
    nf.save_nuclei_pos(fname_pos)
    print("wrote results to '{}'".format(fname_pos))


class InteractiveNucleiFinder:

    def __init__(self, boundingbox: int, min_brightness: int=None,
                 max_brightness: int=None):
        self.boundingbox = boundingbox
        self.min_brightness = min_brightness
        self.max_brightness = max_brightness
        self.nuclei_pos = []
        self.features = None

    def start(self, trajectories: pd.DataFrame, img):
        self.trajectories = trajectories
        self.img = img
        self._start_manual_find()

    def save_nuclei_pos(self, fname):
        df = pd.DataFrame(columns=['x', 'y'])
        for i, pos in enumerate(self.nuclei_pos):
            df.loc[i] = pos
        df.to_csv(fname)

    def _start_manual_find(self):
        posx = self.trajectories.set_index(['frame', 'particle'])['x'].unstack()
        posy = self.trajectories.set_index(['frame', 'particle'])['y'].unstack()

        for pid in posx:
            self.x = posx[pid]
            self.y = posy[pid]
            self.current_bb = get_bounding_box(self.x, self.y, self.boundingbox,
                                               self.img.shape[0], self.img.shape[1])

            self.fig, self.ax = plt.subplots(1, 1)            
            self.fig.suptitle("click on image to mark/unmark nuclei")            
            self.fig.canvas.mpl_connect('button_press_event', self._onclick)
            self.fig.subplots_adjust(bottom=0.23)

            axcolor = 'salmon'
            axcolor2 = 'lightblue'
            ax_sl_min = self.fig.add_axes([0.25, 0.15, 0.65, 0.03], facecolor=axcolor)
            self.slider_min = Slider(ax_sl_min, 'min. brightness', self.img.min(), self.img.max(),
                                     valstep=1, valfmt='%d', color=axcolor2)
            if self.min_brightness is None:
                self.min_brightness = self.img.min()
            self.slider_min.set_val(self.min_brightness)
            self.slider_min.on_changed(self._set_min_brightness)
            ax_sl_max = self.fig.add_axes([0.25, 0.075, 0.65, 0.03], facecolor=axcolor2)
            self.slider_max = Slider(ax_sl_max, 'max. brightness', self.img.min(), self.img.max(),
                                     valstep=1, valfmt='%d', color=axcolor)
            if self.max_brightness is None:
                self.max_brightness = self.img.max()
            self.slider_max.set_val(self.max_brightness)
            self.slider_max.on_changed(self._set_max_brightness)
            self.update()
            plt.show(block=True)

    def _set_min_brightness(self, val):
        if val > self.max_brightness:
            self.slider_max.set_val(val)
        self.min_brightness = val
        self.update()

    def _set_max_brightness(self, val):
        if val < self.min_brightness:
            self.slider_min.set_val(val)
        self.max_brightness = val
        self.update()

    def _onclick(self, event):
        if event.inaxes is None:
            return
        if event.inaxes != self.ax:
            return
        if event.xdata is None:
            return
        x = event.xdata
        y = event.ydata

        i = self._is_close_to_mark(x, y)
        if i is None:
            self.nuclei_pos.append((x, y))
        else:
            self.nuclei_pos.pop(i)
        self.update()

    def _is_close_to_mark(self, x, y):
        for i, pos in enumerate(self.nuclei_pos):
            if abs(pos[0]-x) < 2 and abs(pos[1]-y) < 2:
                return i
        return None
        
    def update(self):
        self.ax.clear()
        img_copy = self.img.copy()
        if self.min_brightness is not None:
            img_copy[img_copy < self.min_brightness] = self.min_brightness
        if self.max_brightness is not None:
            img_copy[img_copy > self.max_brightness] = self.max_brightness
        img_copy = img_copy[self.current_bb[1][0]: self.current_bb[1][1],
                            self.current_bb[0][0]: self.current_bb[0][1]]
        self.ax.imshow(img_copy,
                       # origin='lower',
                       extent=[self.current_bb[0][0], self.current_bb[0][1],
                               self.current_bb[1][0], self.current_bb[1][1]])
        self.ax.plot(self.x, self.y, color='C1')
        x = []
        y = []
        for t in self.nuclei_pos:
            if t[0] < self.current_bb[0][0]:
                continue
            if t[0] > self.current_bb[0][1]:
                continue
            if t[1] < self.current_bb[1][0]:
                continue
            if t[1] > self.current_bb[1][1]:
                continue
            x.append(t[0])
            y.append(t[1])
        self.ax.plot(x, y, 'o', markersize=5, color='C0')
        #invert_yaxis(self.ax)
        self.fig.canvas.draw()
    
@click.command()
@click.argument('analysis-folder')
@click.argument('background-file')
@click.option('--trajectory-bounding-box', '-b', type=int, default=20,
              help="Bounding box in pixels around trajectories.")
@click.option('--min-brightness', '-minb', type=float, default=None)
@click.option('--max-brightness', '-maxb', type=float, default=None)
def main(analysis_folder, background_file, trajectory_bounding_box,
         min_brightness, max_brightness):
    print("{:=^80}".format(' ebtrack-find-nuclei '))
    mpl.rc('figure', figsize=(10, 8))
    mpl.rc('image', cmap='gray')
    find_nuclei(analysis_folder, background_file, trajectory_bounding_box,
                min_brightness, max_brightness)
    print("-"*80)

    
if __name__ == "__main__":
    main()
