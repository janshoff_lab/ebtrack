import os
import click
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import trackpy as tp
from . import results_folder as rf

def vanhove_plot(analysis_folder, lag, bins, microns_per_pixel, frames_per_second):
    print("{:=^80}".format(' ebtrack-vanhove-plot '))
    fname_traj = os.path.join(analysis_folder, 'trajectories.csv')

    results_folder = os.path.abspath(
        os.path.join(analysis_folder, '..'))

    if microns_per_pixel is None or frames_per_second is None:
        try:
            mpp, fps = rf.load_conversion_parameters(results_folder)
        except FileNotFoundError:
            msg = ''.join(["Tried to load conversion parameters (size per ",
                           "pixel and frames per second) from results folder, ",
                           "but they were not found. \n",
                           "Looks like you did not specify them before. ",
                           "You need to specify them at least once. ",
                           "From then on, ebtrack will read them from that file.",
                           "If you provide them again, the old ones will be ",
                           "replaced."])
            print(msg)
            return
        if microns_per_pixel is None:
            microns_per_pixel = mpp
        if frames_per_second is None:
            frames_per_second = fps
    rf.save_conversion_parameters(results_folder, microns_per_pixel,
                                  frames_per_second)
    
    trajectories = pd.read_csv(fname_traj)
    pos_x = trajectories.set_index(['frame', 'particle'])['x'].unstack()
    pos_y = trajectories.set_index(['frame', 'particle'])['y'].unstack()

    # ================ vanhove individual ==================================
    vh_x = tp.vanhove(pos_x, lag, microns_per_pixel, bins=bins)
    vh_y = tp.vanhove(pos_y, lag, microns_per_pixel, bins=bins)

    fig = plt.figure()
    axx = fig.add_subplot(2, 1, 1)
    axy = fig.add_subplot(2, 1, 2, sharex=axx)
    wx = vh_x.index[1] - vh_x.index[0]
    wy = vh_y.index[1] - vh_y.index[0]
    alpha = 115 # 8 bit integer, range [0, 255]
    alpha_str = hex(alpha)[2:].zfill(2)
    colors = [c + alpha_str for c in plt.rcParams['axes.prop_cycle'].by_key()['color']]
    for i, p in enumerate(vh_x):
        axx.bar(vh_x.index, vh_x[p], color=colors[i % len(colors)], edgecolor='black', width=wx)
        axy.bar(vh_y.index, vh_y[p], color=colors[i % len(colors)], edgecolor='black', width=wy)

    axx.set(ylabel='density',
            xlabel='$\Delta x/\mu$m',
            title='individual Van Hove histograms')
    axy.set(ylabel='density',
            xlabel='$\Delta y/\mu$m')    

    fname_vh = os.path.join(analysis_folder,
                            'vanhove_lag_{}_bins_{}.pdf'.format(
                                lag, bins))
    fig.tight_layout()
    fig.savefig(fname_vh)

    # ================ vanhove ensemble =====================================
    vh_x = tp.vanhove(pos_x, lag, microns_per_pixel, ensemble=True, bins=bins)
    vh_y = tp.vanhove(pos_y, lag, microns_per_pixel, ensemble=True, bins=bins)
    
    fig = plt.figure()
    axx = fig.add_subplot(2, 1, 1)
    axy = fig.add_subplot(2, 1, 2, sharex=axx)    
    wx = vh_x.index[1] - vh_x.index[0]
    wy = vh_y.index[1] - vh_y.index[0]
    axx.bar(vh_x.index, vh_x, width=wx)
    axy.bar(vh_y.index, vh_y, width=wy)

    axx.set(ylabel='density',
            xlabel='$\Delta x/\mu$m',
            title='ensemble Van Hove histograms')
    axy.set(ylabel='density',
            xlabel='$\Delta y/\mu$m')
    
    fig.tight_layout()
    fname_vh = fname_vh.replace('vanhove', 'vanhove_ensemble')
    fig.savefig(fname_vh)

    plt.show()


def compute_vanhove_hist_bins(n: int, *displacements) -> np.ndarray:
    """
    Get evenly spaced bin array matching the displacements array. 
    FOR MANUAL VAN HOVE PLOTS, WHEN USING trackpy VAN HOVE UTILITIES DO NOT SUFFICE.
    """
    d = np.concatenate(displacements)
    d_max_abs = max(abs(d.min()), d.max())
    dmin = -d_max_abs
    dmax = d_max_abs
    dx = (dmax-dmin)/(n-1)
    return np.linspace(dmin-dx/2, dmax+dx/2, n+1)
    

def vanhove_hist(displacements: np.ndarray, bins: np.ndarray, ensemble=True) -> np.ndarray:
    """
    Compute Van Hove histogram values.
    FOR MANUAL VAN HOVE PLOTS, WHEN USING trackpy VAN HOVE UTILITIES DO NOT SUFFICE.
    """
    hist = np.full(len(bins)-1, np.nan)
    for i in range(len(bins)-1):
        x0 = bins[i]
        x1 = bins[i+1]
        count = ((displacements >= x0) & (displacements < x1)).sum()
        hist[i] = count
    if not ensemble:
        return hist
    area = (hist * (bins[1:] - bins[:-1])).sum()
    return hist/area
        

    
@click.command()
@click.argument('analysis_folder')
@click.option('--lag', '-l', type=int, default=1)
@click.option('--bins', '-b', type=int, default=24)
@click.option('--microns-per-pixel', '-mpp', type=float, default=None,
              help='If not specified, will try to load from results folder.')
@click.option('--frames-per-second', '-fps', type=float, default=None,
              help='If not specified, will try to load from results folder.')
def main(analysis_folder, lag, bins, microns_per_pixel, frames_per_second):
    vanhove_plot(analysis_folder, lag, bins, microns_per_pixel, frames_per_second)


if __name__ == "__main__":
    main()
