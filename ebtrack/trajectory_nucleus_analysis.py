from typing import List
import os
import click
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from . import results_folder as rf
from .utils import get_bounding_box

BooleanArray = np.ndarray


def trajectory_nucleus_analysis(analysis_folder, distance, background, bounding_box,
                                microns_per_pixel, frames_per_second):
    print("{:=^80}".format(" trajectory-nucleus-analysis "))

    traj_fname = os.path.join(analysis_folder, 'trajectories.csv')
    trajectories = pd.read_csv(traj_fname)
    nucl_fname = os.path.join(analysis_folder, 'nuclei_positions.csv')
    if not os.path.exists(nucl_fname):
        msg = ''.join(["File '{}' not found! ".format(nucl_fname),
                       "You need to run the program 'ebtrack-find-nuclei' ",
                       "on the analysis folder '{}' ".format(analysis_folder),
                       "before you run 'ebtrack-trajectory-nucleus-analysis'."])
        raise FileNotFoundError(msg)
    pos_nucl = pd.read_csv(nucl_fname)

    results_folder = os.path.abspath(
        os.path.join(analysis_folder, '..'))

    if microns_per_pixel is None or frames_per_second is None:
        try:
            mpp, fps = rf.load_conversion_parameters(results_folder)
        except FileNotFoundError:
            msg = ''.join(["Tried to load conversion parameters (size per ",
                           "pixel and frames per second) from results folder, ",
                           "but they were not found. \n",
                           "Looks like you did not specify them before. ",
                           "You need to specify them at least once. ",
                           "From then on, ebtrack will read them from that file.",
                           "If you provide them again, the old ones will be ",
                           "replaced."])
            print(msg)
            return
        if microns_per_pixel is None:
            microns_per_pixel = mpp
        if frames_per_second is None:
            frames_per_second = fps
    rf.save_conversion_parameters(results_folder, microns_per_pixel,
                                  frames_per_second)
    
    dist_in_px = distance/microns_per_pixel

    masks = get_masks_close_to_nucleus(trajectories, pos_nucl, dist_in_px)

    pos_x = trajectories.set_index(['frame', 'particle'])['x'].unstack()
    pos_y = trajectories.set_index(['frame', 'particle'])['y'].unstack()

    if background is not None:
        background = plt.imread(background)

    outfolder = os.path.join(analysis_folder, 'traj_plus_nuclei')
    if not os.path.exists(outfolder):
        os.mkdir(outfolder)

    for i, pid in enumerate(pos_x):
        xy = np.full((len(pos_x[pid]), 2), np.nan)
        xy[:, 0] = pos_x[pid]
        xy[:, 1] = pos_y[pid]    
        fig = plot_masked_trajectory(analysis_folder, xy, masks[i], pos_nucl,
                                     background, bounding_box)
        
        fig.savefig(os.path.join(outfolder, 'particle_{}.pdf'.format(pid)))
    plt.show()
        
    print('-'*80)

def plot_masked_trajectory(analysis_folder: str, trajectory: np.ndarray,
                           mask: List[BooleanArray], pos_nucl, background: np.ndarray,
                           bounding_box: int) -> plt.Figure:

    fig, ax = plt.subplots(1, 1)

    w = pos_nucl['x'].max()
    h = pos_nucl['y'].max()
    
    if background is not None:
        ax.imshow(background, alpha=0.8)
        w = background.shape[1]
        h = background.shape[0]
        
    ax.plot(pos_nucl['x'], pos_nucl['y'], 'o')

    ax.plot(trajectory[mask, 0], trajectory[mask, 1])
    ax.plot(trajectory[~mask, 0], trajectory[~mask, 1])

    bb = get_bounding_box(trajectory[:, 0], trajectory[:, 1], bounding_box,
                          h, w)
    ax.set(xlim=bb[0], ylim=bb[1])
    
    return fig
    
    
def get_masks_close_to_nucleus(trajectories: pd.DataFrame,
                               pos_nucl: pd.DataFrame,
                               distance: float) -> List[BooleanArray]:

    distance_squared = distance**2
    pos_x = trajectories.set_index(['frame', 'particle'])['x'].unstack()
    pos_y = trajectories.set_index(['frame', 'particle'])['y'].unstack()
    masks = []

    xy_nucl = np.empty((len(pos_nucl['x']), 2))
    xy_nucl[:, 0] = pos_nucl['x']
    xy_nucl[:, 1] = pos_nucl['y']

    for pid in pos_x:
        m = []
        xy = np.full((len(pos_x[pid]), 2), np.nan)
        xy[:, 0] = pos_x[pid]
        xy[:, 1] = pos_y[pid]

        for xyi in xy:
            d_squared = ((xy_nucl-xyi)**2).sum(1)
            if (d_squared <= distance_squared).any():
                m.append(True)
            else:
                m.append(False)
        masks.append(np.array(m))
    return masks


@click.command()
@click.argument('analysis_folder')
@click.argument('distance', type=float)
@click.option('--background', '-bg', type=click.Path(exists=True), default=None)
@click.option('--bounding-box', '-bb', type=int, default=30)
@click.option('--microns-per-pixel', '-mpp', type=float, default=None,
              help='If not specified, will try to load from results folder.')
@click.option('--frames-per-second', '-fps', type=float, default=None,
              help='If not specified, will try to load from results folder.')
def main(analysis_folder, distance, background, bounding_box,
         microns_per_pixel, frames_per_second):    
    """
    Performs trajectory analyses with each point of a trajectory
    classified as either close or not close to a nuclues.
    Distance needs to be specified in physical length, the conversion
    the conversion to size in pixels will be performed with the provided
    value for size_per_pixel.
    """
    trajectory_nucleus_analysis(analysis_folder, distance,
                                background, bounding_box,
                                microns_per_pixel, frames_per_second)


if __name__ == "__main__":
    main()
